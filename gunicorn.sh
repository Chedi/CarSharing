#!/bin/bash

USER=root
GROUP=root
NUM_WORKERS=4
NAME="Mitfahrgelegenheit"
DJANGO_WSGI_MODULE=Mitfahrgelegenheit.wsgi
DJANGO_SETTINGS_MODULE=Mitfahrgelegenheit.settings

DJANGODIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
PROJECTDIR=$(readlink -f $(dirname ${DJANGODIR}))
SOCKFILE=$PROJECTDIR/run/gunicorn.sock

echo "Starting $NAME"

cd $DJANGODIR
source $PROJECTDIR/ve/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME                                  \
  --workers $NUM_WORKERS                        \
  --user=$USER --group=$GROUP                   \
  --log-level=debug                             \
  -t 60000                                      \
  --bind=unix:$SOCKFILE