from cities.models  import Region
from rest_framework import serializers

from teilen.serializers.cities import country


class Serializer(serializers.ModelSerializer):
    country = country.Serializer

    class Meta:
        model  = Region
        fields = ('id', 'name', 'name_std', 'country')


class DetailsSerializer(serializers.ModelSerializer):
    country = country.Serializer()

    class Meta:
        model  = Region
        fields = ('id', 'name', 'name_std', 'country')
