from cities.models  import Country
from rest_framework import serializers


class Serializer(serializers.ModelSerializer):

    class Meta:
        model  = Country
        fields = ('id', 'name', 'code', 'continent')
