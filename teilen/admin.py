from django.contrib import admin

from forms import JourneyForm
from forms import CustomerForm
from forms import PassengersForm

from teilen.models import Journey
from teilen.models import Customer
from teilen.models import Passengers

from django.utils.translation import ugettext as _


class CustomerAdmin(admin.ModelAdmin):
    form          = CustomerForm
    sortable      = ['username', 'first_name', 'last_name', 'email', 'is_active', ]
    list_filter   = ['is_active', 'is_staff', 'is_superuser']
    list_display  = ['username', 'first_name', 'last_name', 'email', 'city', 'is_active', 'is_staff', 'is_superuser']
    search_fields = ['username', 'first_name', 'last_name', 'email', 'city__name', 'city__name_std']
    list_editable = ['is_active', ]

    fields  = [
        'first_name',
        'last_name',
        'username',
        'email',
        'city',
        'picture',
        'is_active',
        'is_staff',
        'is_superuser',
    ]


class PassengerInline(admin.StackedInline):
    extra               = 0
    form                = PassengersForm
    model               = Passengers
    verbose_name        = _('Passenger')
    verbose_name_plural = _('Passengers')


class JourneyAdmin(admin.ModelAdmin):
    form          = JourneyForm
    sortable      = ['departure_date', 'arrival_date', 'price', ]
    list_filter   = ['departure_date', 'arrival_date', 'price', ]
    list_display  = ['departure_city', 'arrival_city', 'departure_date', 'arrival_date', 'price', ]
    inlines       = [PassengerInline, ]
    search_fields = [
        'departure_city__name',
        'arrival_city__name',
        'departure_city__name_std',
        'arrival_city__name_std',
        'passengers__username',
        'passengers__first_name',
        'passengers__last_name',
        'passengers__email'
    ]


admin.site.register(Journey   , JourneyAdmin   )
admin.site.register(Customer  , CustomerAdmin  )
