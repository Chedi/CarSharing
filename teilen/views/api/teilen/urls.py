from django.conf.urls import url
from django.conf.urls import patterns

from teilen.views.api.teilen.views import ProfileList
from teilen.views.api.teilen.views import CustomersList
from teilen.views.api.teilen.views import CustomersAuto
from teilen.views.api.teilen.views import CustomersDetail
from teilen.views.api.teilen.views import CustomersRetrive
from teilen.views.api.teilen.views import ProfileSharedList
from teilen.views.api.teilen.views import ProfileJourneysList


urlpatterns = patterns(
    '',
    url(r'^profile/$'                    , ProfileList        , name='customer_profile_api'         ),
    url(r'^profile/shared/$'             , ProfileSharedList  , name='customer_profile_shared_api'  ),
    url(r'^profile/journeys/$'           , ProfileJourneysList, name='customer_profile_journyes_api'),

    url(r'^customer/$'                   , CustomersList      , name='customers_listing_api'        ),
    url(r'^customer/(?P<pk>\d+)/$'       , CustomersRetrive   , name='customers_detail_api'         ),
    url(r'^customer/(?P<pk>\d+)/detail/$', CustomersDetail    , name='customers_highlight_api'      ),
    url(r'^customer/autocomplete/$'      , CustomersAuto      , name='customers_autocomplete_api'   ),
)
