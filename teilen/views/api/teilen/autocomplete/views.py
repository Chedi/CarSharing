from rest_framework.views     import APIView
from rest_framework.response  import Response
from rest_framework.renderers import JSONRenderer

from django.db.models  import Q
from teilen.models     import Customer

from teilen.views.api.common.utils      import autocomplete_helper
from teilen.serializers.teilen.customer import Serializer as CustomerSerializer


class CustomersAutocomplete(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        pattern, rows = autocomplete_helper(request)

        return Response(
            CustomerSerializer(
                Customer.objects.filter(
                    Q(is_staff            =False   ) &
                    Q(is_active           =True    ) &
                    Q(is_superuser        =False   ) &
                    Q(username__contains  =parttern) |
                    Q(last_name__contains =parttern) |
                    Q(first_name__contains=parttern))[:rows]
            ).data)
