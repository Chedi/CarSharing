from cities.models     import City
from cities.models     import Region
from cities.models     import Country
from django.db.models  import Q

from rest_framework.views     import APIView
from rest_framework.response  import Response
from rest_framework.renderers import JSONRenderer

from teilen.views.api.common.utils import autocomplete_helper

from teilen.serializers.cities.city    import Serializer as CitySerializer
from teilen.serializers.cities.region  import Serializer as RegionSerializer
from teilen.serializers.cities.country import Serializer as CountrySerializer


class CitiesAutocomplete(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        pattern, rows = autocomplete_helper(request)

        return Response(
            CitySerializer(
                City.objects.filter(Q(name__contains=pattern) | Q(name_std__contains=pattern))[:rows]
            ).data)


class RegionsAutocomplete(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        pattern, rows = autocomplete_helper(request)

        return Response(
            RegionSerializer(
                Region.objects.filter(Q(name__contains=pattern) | Q(name_std__contains=pattern))[:rows]
            ).data)


class CountriesAutocomplete(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):
        pattern, rows = autocomplete_helper(request)

        return Response(
            CountrySerializer(
                Country.objects.filter(Q(name__contains=pattern) | Q(code__contains=pattern))[:rows]
            ).data)
