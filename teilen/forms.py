from django import forms
import autocomplete_light
from models import Journey
from models import Customer
from models import Passengers
from cities.models import City
from django.forms import ModelForm
from suit.widgets import SuitSplitDateTimeWidget
from django.utils.translation import ugettext as _


autocomplete_light.register(
    City,
    search_fields=['^name', 'slug', 'region__name', 'subregion__name', ],
    autocomplete_js_attributes={'placeholder': _('City ?'), },
)

autocomplete_light.register(
    Customer,
    search_fields=['^first_name', 'last_name', 'username', 'email', ],
    autocomplete_js_attributes={'placeholder': _('Customer ?'), },
)


class CustomerForm(ModelForm):
    class Meta:
        model   = Customer
        widgets = {
            'password': forms.PasswordInput,
            'city'    : autocomplete_light.ChoiceWidget('CityAutocomplete')
        }


class JourneyForm(ModelForm):
    class Meta:
        model   = Journey
        widgets = {
            'arrival_date'  : SuitSplitDateTimeWidget,
            'departure_date': SuitSplitDateTimeWidget,
            'arrival_city'  : autocomplete_light.ChoiceWidget('CityAutocomplete'),
            'departure_city': autocomplete_light.ChoiceWidget('CityAutocomplete'),
        }


class PassengersForm(ModelForm):
    class Meta:
        model   = Passengers
        widgets = {
            'customer': autocomplete_light.ChoiceWidget('CustomerAutocomplete')
        }
